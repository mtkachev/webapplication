package com.tkachov.controllers;

import com.tkachov.dto.ProductDto;
import com.tkachov.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/**
 * Created by Max on 18.09.2016.
 */
@Controller
public class EditProduct{

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ModelAndView editProduct(@Validated ProductDto product) {
        ModelAndView view = new ModelAndView();
        if (product.getName().trim().length() > 0 && product.getPrice() > 0){
            productService.updateProduct(product);
            }
            view.setViewName("redirect:/home");
        return view;
        }
}

