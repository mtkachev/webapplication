package com.tkachov.controllers;

import com.tkachov.dto.UserDto;
import com.tkachov.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * Created by Max on 27.09.2016.
 */
@Controller
public class Login {
    
    @Autowired
    UserService userService;
        
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView openLoginForm(){
        return new ModelAndView("login");
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView login(HttpSession session, UserDto userDto) {
        //TODO hibernate validation @Valid
        ModelAndView view = new ModelAndView("login");
        UserDto user = userService.getUserByLoginName(userDto);
        if (user != null) {
            session.setAttribute("user", user);
            view.setViewName("redirect:/home");
        } else {
            view.addObject("error",  "Invalid username or password");
        }
        return view;
    }
    
}
