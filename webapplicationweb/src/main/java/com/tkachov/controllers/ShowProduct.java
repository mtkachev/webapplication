package com.tkachov.controllers;

import com.tkachov.dto.ProductDto;
import com.tkachov.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Max on 13.09.2016.
 */
@Controller
public class ShowProduct {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView showProducts() {
        List<ProductDto> productList = productService.getAllProducts();
        ModelAndView showProductPage = new ModelAndView("showProduct");
        showProductPage.addObject("products", productList);
        return showProductPage;
    }

    @RequestMapping(value = "/home", method = RequestMethod.POST)
    public ModelAndView applyActionToProduct(
            @RequestParam("action") String action,
            @RequestParam(value = "checked", required = false) List<Integer> checked) {
        ModelAndView view;
        ProductDto product;
        switch (action) {
            case "Refresh":
                view = showProducts();
                break;
            case "Delete":
                if (checked!=null && checked.size() > 0) {
                    productService.deleteProduct(checked);
                    view = showProducts();
                }
                else {
                     view = new ModelAndView("error");
                     view.addObject("errorMessage", "Please check at least one checkbox to delete product");
                }
                break;
            case "Edit":
                if (checked!=null && checked.size()==1){
                    product = productService.getProductById(checked.get(0));
                    view = new ModelAndView("editProduct");
                    view.addObject("product", product);
                }
                else {
                    view = new ModelAndView("error");
                    view.addObject("errorMessage", "Please check only one checkbox to edit product");
                }
                break;
            case "Add":
                product = new ProductDto();
                view = new ModelAndView("editProduct");
                view.addObject("product", product);
                break;
            default:
                view = showProducts();
        }
        return view;
    }
}
