package com.tkachov.dao;

import com.tkachov.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Max on 27.09.2016.
 */
@Repository
public interface UserDao extends CrudRepository<User, Integer>{
    User getUserByLogin(String login);
}
