package com.tkachov.dao;

import com.tkachov.entities.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by Max on 17.09.2016.
 */
@Repository
public interface ProductDao extends CrudRepository<Product, Integer> {
}
