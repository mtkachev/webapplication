package com.tkachov.dto;


import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

/**
 * Created by Max on 16.09.2016.
 */

public class ProductDto {

    private int id;
    @NotEmpty(message = "The name is required field")
    @Size(max = 30)
    private String name;

    @Digits(integer = 10 /*precision*/, fraction = 2 /*scale*/)
    private double price;

    public ProductDto(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public ProductDto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProductDto other = (ProductDto) obj;
        if (id != other.id)
            return false;
        if (!name.equals(other.name))
            return false;
        if (price != other.getPrice())
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
