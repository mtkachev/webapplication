package com.tkachov.services;

import com.tkachov.dao.ProductDao;
import com.tkachov.dto.ProductDto;
import com.tkachov.entities.Product;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Max on 17.09.2016.
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private Mapper mapper;

    @Autowired
    private ProductDao productDao;

    @Override
    public List<ProductDto> getAllProducts() {
        Iterable<Product> products = productDao.findAll();
        List<ProductDto> productList = new ArrayList<>();
        products.forEach(product -> {
            ProductDto productDto  = mapper.map(product, ProductDto.class);
                    productDto.setName(product.getName().toUpperCase());
                    productList.add(productDto);
        });
        return productList;
    }

    @Override
    public void updateProduct(final ProductDto productDto) {
        Product product = mapper.map(productDto, Product.class);
        productDao.save(product);
    }

    @Override
    public void deleteProduct(final List<Integer> idsStr) {
        idsStr.forEach(id -> productDao.deleteById(id));

    }

    @Override
    public ProductDto getProductById(final Integer id) {
        Product product = productDao.findById(id).orElse(null);

        if (product!=null) {
            ProductDto productDto = mapper.map(product, ProductDto.class);
            productDto.setName(product.getName().toUpperCase());
            return productDto;
        } else {
            return null;
        }
    }
}
