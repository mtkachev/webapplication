package com.tkachov.services;

import com.tkachov.dto.UserDto;
import com.tkachov.entities.User;
import com.tkachov.dao.UserDao;
import com.tkachov.utils.StringUtil;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Max on 27.09.2016.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private Mapper mapper;
    
    @Autowired
    private UserDao userDao;

    @Override
    public UserDto getUserByLoginName(UserDto userDto) {
        //TODO check userDto on null
        User user = userDao.getUserByLogin(userDto.getLogin());
        if (user != null) {
            String password = user.getPassword();
            if (!StringUtil.isEmpty(password) && password.equals(userDto.getPassword())) {
                return mapper.map(user, UserDto.class);
            }
            return null;
        }
        return null;
    }
}
