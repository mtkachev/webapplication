package com.tkachov.services;

import com.tkachov.dto.ProductDto;

import java.util.List;

/**
 * Created by Max on 17.09.2016.
 */
public interface ProductService {
    List<ProductDto> getAllProducts();

    void updateProduct(ProductDto productDto);

    void deleteProduct(List<Integer> strIds);

    ProductDto getProductById(Integer id);
}
