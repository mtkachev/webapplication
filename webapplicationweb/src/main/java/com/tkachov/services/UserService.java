package com.tkachov.services;

import com.tkachov.dto.UserDto;

/**
 * Created by Max on 27.09.2016.
 */
public interface UserService {

    UserDto getUserByLoginName(UserDto userDto);
}
