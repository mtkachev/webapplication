package com.tkachov.utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Max on 21.09.2016.
 */
public class ReadFileUtil {

    public static String readFromFile(String fileName) {
        Stream<String> stream = null;
        try {
            stream = Files.lines(Paths.get(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (stream != null) {
            return stream.collect(Collectors.joining());
        }
        return "";
    }
}
