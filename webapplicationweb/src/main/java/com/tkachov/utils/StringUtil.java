package com.tkachov.utils;

/**
 * Created by Max on 20.09.2016.
 */
public class StringUtil {

    public static boolean isNumber(final String number) {
        if (number == null) {
            return false;
        }
        final char[] charArray = number.toCharArray();
        for (final char c : charArray) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isEmpty(final String param) {
        boolean isEmpty = false;
        if ((param == null) || (param.trim().length() == 0)) {
            isEmpty = true;
        }
        return isEmpty;
    }
}
