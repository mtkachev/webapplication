/**
 * Created by Max on 18.09.2016.
 */
function validateEmptyField() {
    var inputFields = $(".text-field");
    var empty = [];
    inputFields.each(function () {
        if ($(this).val() == '') {
            empty.push(this);
        }
    });
    if (empty.length > 0) {
        alert("TextFields can not be empty.");
        return false;
    }
}

$(document).ready(function () {
    $(".save").click(function () {
        return validateEmptyField();
    })
});