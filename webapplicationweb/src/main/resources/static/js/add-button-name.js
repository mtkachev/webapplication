function changeLabelAddButton(){
    var addButton = jQuery("#Add");
    var checkBoxes = $(".check-row");
    var checked = [];
    checkBoxes.each(function () {
        if ($(this).is(':checked')) {
            checked.push(this);
        }
    });
    if (checked.length > 0) {
        addButton.val("Edit");
    }
    else {
        addButton.val("Add");
    }

}