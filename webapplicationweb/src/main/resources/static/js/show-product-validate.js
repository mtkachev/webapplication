/**
 * Created by Max on 18.09.2016.
 */
function validate() {
    var checkBoxes = $(".check-row");
    var checked = [];
    checkBoxes.each(function () {
        if ($(this).is(':checked')) {
            checked.push(this);
        }
    });
    if (checked.length > 1) {
        alert("More than two products selected.");
        return false;
    }
}
function validateDelete() {
    var checkBoxes = $(".check-row");
    var checked = [];
    checkBoxes.each(function () {
        if ($(this).is(':checked')) {
            checked.push(this);
        }
    });
    if (checked.length <= 0) {
        alert("Please choose at least one item.");
        return false;
    }
}
$(document).ready(function () {
    $(".edit").click(function () {
        return validate();
    });
    $(".delete").click(function () {
        return validateDelete();
    })
});