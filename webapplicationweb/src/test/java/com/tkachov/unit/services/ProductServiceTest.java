package com.tkachov.unit.services;

import com.tkachov.services.ProductService;
import com.tkachov.unit.ParentTest;
import com.tkachov.dao.ProductDao;
import com.tkachov.dto.ProductDto;
import com.tkachov.entities.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

/**
 * Created by Max on 21.09.2016.
 */

public class ProductServiceTest extends ParentTest {

    private List<Product> mockedProducts = new ArrayList<>();

    @MockBean
    private ProductDao mockedProductDao;

    @Autowired
    ProductService productService;

    private final String ERROR_MESSAGE = "The products are not equals";

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        mockedProducts.add(new Product(1, "test1", 1.0));
        mockedProducts.add(new Product(2, "test2", 2.0));
        mockedProducts.add(new Product(3, "test3", 3.0));
        when(mockedProductDao.findAll()).thenReturn(mockedProducts);
        when(mockedProductDao.findById(3)).thenReturn(Optional.ofNullable(mockedProducts.get(2)));
    }
    
    @Test
    public void testGetAllProductsTest() {
        List<ProductDto> expectedProducts = new ArrayList<>();
        expectedProducts.add(new ProductDto(1, "TEST1", 1.0));
        expectedProducts.add(new ProductDto(2, "TEST2", 2.0));
        expectedProducts.add(new ProductDto(3, "TEST3", 3.0));

        List<ProductDto> actualProducts = productService.getAllProducts();
        Assert.assertEquals("Counts of products are not equals", expectedProducts.size(), actualProducts.size());
        Assert.assertEquals(ERROR_MESSAGE, expectedProducts, actualProducts);
    }

    @Test
    public void testGetProductById() {
        ProductDto expectedProduct = new ProductDto(3, "TEST3", 3.0);
        ProductDto actualProduct = productService.getProductById(3);
        Assert.assertEquals(ERROR_MESSAGE, expectedProduct, actualProduct);
        actualProduct = productService.getProductById(1);
        Assert.assertNull(ERROR_MESSAGE, actualProduct);
    }
}
