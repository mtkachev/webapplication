package com.tkachov.unit.controllers;

import com.tkachov.unit.ParentTest;
import com.tkachov.dto.ProductDto;
import com.tkachov.services.ProductService;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@AutoConfigureMockMvc
public class ShowProductControllerTest extends ParentTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService mockedProductService;

    @Test
    public void testShowProducts() throws Exception {
        mockMvc.perform(get("/home"))
                .andExpect(status().isOk())
                .andExpect(view().name("showProduct"))
                .andExpect(model().attributeExists("products"));
    }

    @Test
    public void testApplyRefreshToProduct() throws Exception {
        mockMvc.perform(post("/home")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("action=Refresh"))
                .andExpect(status().isOk())
                .andExpect(view().name("showProduct"))
                .andExpect(model().attributeExists("products"));
    }

    @Test
    public void testApplyDeleteToProduct() throws Exception {
        mockMvc.perform(post("/home")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("action=Delete"))
                .andExpect(status().isOk())
                .andExpect(view().name("error"))
                .andExpect(model().attribute("errorMessage","Please check at least one checkbox to delete product"));
    }

    @Test
    public void testApplyEditToProduct() throws Exception {
        mockMvc.perform(post("/home")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("action=Edit"))
                .andExpect(status().isOk())
                .andExpect(view().name("error"))
                .andExpect(model().attribute("errorMessage","Please check only one checkbox to edit product"));
    }

    @Test
    public void testApplyAddToProduct() throws Exception {
        mockMvc.perform(post("/home")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("action=Add"))
                .andExpect(status().isOk())
                .andExpect(view().name("editProduct"))
                .andExpect(model().attributeExists("product"));
    }

    @Test
    public void testInvalidActionToProduct() throws Exception {
        mockMvc.perform(post("/home")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("action=E"))
                .andExpect(status().isOk())
                .andExpect(view().name("showProduct"))
                .andExpect(model().attributeExists("products"));
    }

    @Test
    public void testApplyEditAndOneCheckedItemToProduct() throws Exception {
        String name = "Username";
        double price = 999.2;
        int id = 1;
        ProductDto productDto = new ProductDto();
        productDto.setName(name);
        productDto.setPrice(price);
        productDto.setId(id);
        Mockito.when(mockedProductService.getProductById(1)).thenReturn(productDto);
        mockMvc.perform(post("/home")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("action=Edit&checked=1"))
                .andExpect(status().isOk())
                .andExpect(view().name("editProduct"))
                .andExpect(model().attribute("product", hasProperty("id", is(id))))
                .andExpect(model().attribute("product", hasProperty("name", is(name))))
                .andExpect(model().attribute("product", hasProperty("price", is(price))));

    }

    @Test
    public void testApplyEditAndTwoCheckedItemToProduct() throws Exception {
        mockMvc.perform(post("/home")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("action=Edit&checked=1&checked=2"))
                .andExpect(status().isOk())
                .andExpect(view().name("error"))
                .andExpect(model().attribute("errorMessage","Please check only one checkbox to edit product"));


    }

    @Test
    public void testApplyAddAndOneCheckedItemToProduct() throws Exception {
        mockMvc.perform(post("/home")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("action=Add&checked=1"))
                .andExpect(status().isOk())
                .andExpect(view().name("editProduct"))
                .andExpect(model().attribute("product", hasProperty("id", is(0))))
                .andExpect(model().attribute("product", hasProperty("name", is(nullValue()))))
                .andExpect(model().attribute("product", hasProperty("price", is(0.0))));

    }

    @Test
    public void testApplyRefreshAndOneCheckedItemToProduct() throws Exception {
        mockMvc.perform(post("/home")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("action=Refresh&checked=1"))
                .andExpect(status().isOk())
                .andExpect(view().name("showProduct"))
                .andExpect(model().attributeExists("products"));
    }

    @Test
    public void testApplyDeleteAndOneCheckedItemToProduct() throws Exception {
        mockMvc.perform(post("/home")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("action=Delete&checked=1"))
                .andExpect(status().isOk())
                .andExpect(view().name("showProduct"))
                .andExpect(model().attributeExists("products"));
    }


}
