package com.tkachov.unit.controllers;

import com.tkachov.unit.ParentTest;
import com.tkachov.services.ProductService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@AutoConfigureMockMvc
public class EditProductControllerTest extends ParentTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService mockedProductService;

    @Test
    public void testAddProduct() throws Exception {
        mockMvc.perform(post("/edit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("id=999&name=123456789012345678901234567890&price=1234567890.12"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/home"));
    }

    @Test
    public void testEmptyNameProduct() throws Exception {
        mockMvc.perform(post("/edit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("id=999&name=&price=999"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testNullNameProduct() throws Exception {
        mockMvc.perform(post("/edit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("id=999&price=999"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testOverSizeNameProduct() throws Exception {
        mockMvc.perform(post("/edit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("name=1234567890123456789012345678901&price=999"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testOverPriceProduct() throws Exception {
        mockMvc.perform(post("/edit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("id=999&name=123456789012345678901234567890&price=12345678901.12"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testOverPriceScaleProduct() throws Exception {
        mockMvc.perform(post("/edit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).content("id=999&name=123456789012345678901234567890&price=1234.123"))
                .andExpect(status().is4xxClientError());
    }

}
