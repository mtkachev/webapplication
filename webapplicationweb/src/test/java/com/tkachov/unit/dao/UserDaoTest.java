package com.tkachov.unit.dao;

import com.tkachov.dao.UserDao;
import com.tkachov.unit.ParentTest;
import com.tkachov.entities.User;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Max on 21.09.2016.
 */

public class UserDaoTest extends ParentTest {

    @Autowired
    UserDao userDao;
    private static Connection connection = getConnection();

    private static Connection getConnection() {
        try {
            Class.forName("org.h2.Driver");
            Connection connection = DriverManager.getConnection("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", "test", "");
            return connection;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    @BeforeClass
    public static void initEmbeddedDB() {
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS users(id INT AUTO_INCREMENT PRIMARY KEY, login VARCHAR(30), password VARCHAR (20), first_name VARCHAR (20), last_name VARCHAR (20))");
            stmt.executeUpdate("INSERT INTO users (id, login, password, first_name, last_name) VALUES (1,'admin','admin','Max','Tkachov')");
            stmt.close();
        } catch ( SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetValidUserByLogin() {
        User expectedUser = new User();
        expectedUser.setId(1);
        expectedUser.setLogin("admin");
        expectedUser.setPassword("admin");
        expectedUser.setFirstName("Max");
        expectedUser.setLastName("Tkachov");
        User actualUser = userDao.getUserByLogin("admin");
        Assert.assertEquals("User with login admin is not presented in DB", expectedUser, actualUser);
    }

    @Test
    public void testGetInvalidUserByLogin() {
        User actualUser = userDao.getUserByLogin("admi");
        Assert.assertNull("User with invalid login is being returned", actualUser);
    }
}
