package com.tkachov.integration.serenitybdd.features.record_items;


import com.tkachov.integration.serenitybdd.questions.TheTodoItems;
import com.tkachov.integration.serenitybdd.tasks.AddATodoItem;
import com.tkachov.integration.serenitybdd.tasks.StartWith;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.hasItem;

@RunWith(SerenityRunner.class)
public class AddItem {

    Actor max = Actor.named("Max");

    @Managed
    public WebDriver hisBrowser;

    @Before
    public void maxCanBrowseTheWeb(){
        max.can(BrowseTheWeb.with(hisBrowser));
    }

    @Test
    public void should_be_able_to_add_an_item_to_the_todo_list() {
        givenThat(max).wasAbleTo(StartWith.anEmptyTodoList());
        when(max).attemptsTo(AddATodoItem.called("Feed the cat"));
        then(max).should(GivenWhenThen.seeThat(TheTodoItems.displayed(), hasItem("Feed the cat")));
    }
}
