package com.tkachov.integration.serenitybdd.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import com.tkachov.integration.serenitybdd.ui.TheTodoList;

import java.util.List;

public class TheTodoItems implements Question<List<String>> {

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(TheTodoList.ITEMS)
                .viewedBy(actor)
                .asList();
    }

    public static Question<List<String>> displayed() {
        return new TheTodoItems();
    }
}
