package com.tkachov.integration.serenitybdd.ui;

import net.serenitybdd.screenplay.targets.Target;

public class LoginPage {

    public static Target LOGIN_INPUT
            = Target.the("Login Input").locatedBy("#loginInput");

    public static Target PASSWORD_INPUT
            = Target.the("Password Input").locatedBy("#passwordInput");

    public static Target LOGIN_BUTTON
             = Target.the("Login Button").locatedBy("#loginButton");
}
