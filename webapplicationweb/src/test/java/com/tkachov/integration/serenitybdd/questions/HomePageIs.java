package com.tkachov.integration.serenitybdd.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.annotations.Subject;

@Subject("The opened HomePage")
public class HomePageIs implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        return BrowseTheWeb.as(actor).compatibleWithUrl("/home");
    }

    public static Question<Boolean> opened() {
        return new HomePageIs();
    }
}
