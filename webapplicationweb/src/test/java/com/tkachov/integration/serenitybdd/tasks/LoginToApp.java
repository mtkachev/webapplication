package com.tkachov.integration.serenitybdd.tasks;

import com.tkachov.integration.serenitybdd.ui.LoginPage;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

public class LoginToApp implements Task {

    private final String login;
    private final String password;

    public LoginToApp(String login, String password){
        this.login = login;
        this.password = password;
    }

    @Override
    @Step("Logs in as: {0}")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(login)
                        .into(LoginPage.LOGIN_INPUT),
                Enter.theValue(password)
                        .into(LoginPage.PASSWORD_INPUT),
                Click.on(LoginPage.LOGIN_BUTTON));

    }

    public static Task withCredentials(String login, String password) {
        return Instrumented.instanceOf(LoginToApp.class)
                .withProperties(login, password);
    }
}
