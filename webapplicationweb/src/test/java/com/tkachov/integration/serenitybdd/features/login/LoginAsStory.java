package com.tkachov.integration.serenitybdd.features.login;

import com.tkachov.integration.serenitybdd.questions.HomePageIs;
import com.tkachov.integration.serenitybdd.tasks.LoginToApp;
import com.tkachov.integration.serenitybdd.tasks.OpenTheApplication;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.*;

@RunWith(SerenityRunner.class)
public class LoginAsStory {

    private Actor admin = new Actor("admin");

    @Managed
    private WebDriver hisBrowser;

    @Steps
    private
    OpenTheApplication openTheApplication;

    @Before
    public void adminCanBrowseTheWeb(){
        admin.can(BrowseTheWeb.with(hisBrowser));
    }

    @Test
    public void should_be_login_as_admin() {
        givenThat(admin).wasAbleTo(openTheApplication);

        when(admin).attemptsTo(LoginToApp.withCredentials("admin", "admin"));

        then(admin).should(seeThat(HomePageIs.opened()));
    }
}
