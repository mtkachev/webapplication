package com.tkachov.integration.serenitybdd.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import com.tkachov.integration.serenitybdd.ui.TheTodoList;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Keys;

public class AddATodoItem implements Task {

    private final String itemName;

    public AddATodoItem (String itemName){
        this.itemName = itemName;
    }

    @Override
    @Step("{0} adds an item called '#itemName'")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(itemName)
                        .into(TheTodoList.WHAT_NEEDS_TO_BE_DONE)
                        .thenHit(Keys.RETURN));

    }

    public static Task called(String itemName) {
        return Instrumented.instanceOf(AddATodoItem.class)
                .withProperties(itemName);
    }
}
