package com.tkachov.integration.serenitybdd.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import com.tkachov.integration.serenitybdd.ui.AppLoginPage;

public class OpenTheApplication implements Task {

    AppLoginPage appLoginPage;

    @Step("Open the application")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(appLoginPage)
        );
    }
}
