package com.tkachov.integration.serenitybdd.ui;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:8080")
public class AppLoginPage extends PageObject {}
